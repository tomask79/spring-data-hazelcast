# Spring Data with Hazelcast as Spring Data Repository #

In our payment system we use very simple way of caching. We've got local [EhCache](https://www.ehcache.org/), which is fine, but feeded via [JDBC](https://en.wikipedia.org/wiki/Java_Database_Connectivity) layer.
What are the drawbacks of this design:

- It's a local cache. No data changes propagation to the other nodes.
- No JPA involved.

....and others. So I am looking for a switch. Nice solution to the mentioned problems is [Hazelcast](https://hazelcast.org/) wrapped into [Spring Data API](https://spring.io/projects/spring-data)
called [spring-data-hazelcast](https://github.com/hazelcast/spring-data-hazelcast) 

In this repository I will show you howto build full [read-through](https://www.ehcache.org/documentation/3.3/caching-patterns.html#read-through) cache with Hazelcast Spring Data API backed by H2 database.

### This is the goal 

![Target](https://github.com/hazelcast/hazelcast-code-samples/blob/master/hazelcast-integration/spring-data-jpa-hazelcast-migration/src/site/markdown/images/after-arch.png?raw=true){width=640 height=480}

At first, definitely check this [spring-data-jpa-hazelcast-migration](https://github.com/hazelcast/hazelcast-code-samples/tree/master/hazelcast-integration/spring-data-jpa-hazelcast-migration)
If you're a Spring Data JPA newbie then this guide is very helpful. But if Spring Boot, JPA or Spring Data is a routine for you then this guide is frustrating a little bit,
because for example it doesn't say anything about howto connect hazelcast with database to have this architecture to be working in the read-through manner. So to complete the author's
idea be sure you're familiar with [Hazelcast MapLoader](https://docs.hazelcast.org/docs/latest/javadoc/com/hazelcast/map/MapLoader.html)

### Read-through caching with Hazelcast step by step

**First step**

**maven dependencies**

	<properties>
		<java.version>1.8</java.version>
		<spring-data-hazelcast-version>2.2.5</spring-data-hazelcast-version>
	</properties>

	<dependencies>
		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter</artifactId>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-data-jpa</artifactId>
		</dependency>

		<dependency>
			<groupId>com.h2database</groupId>
			<artifactId>h2</artifactId>
			<scope>runtime</scope>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-web</artifactId>
		</dependency>

		<dependency>
			<groupId>com.hazelcast</groupId>
			<artifactId>spring-data-hazelcast</artifactId>
			<version>${spring-data-hazelcast-version}</version>
		</dependency>

		<dependency>
			<groupId>org.springframework.boot</groupId>
			<artifactId>spring-boot-starter-test</artifactId>
			<scope>test</scope>
		</dependency>
	</dependencies>

**spring-data-hazelcast** artifact already comes with hazelcast packed inside, so no need to be worried about versions problems. **Big Like!**

**Second step**

**cached entity** stored in H2 database and provided to hazelcast via mentioned MapLoader, which is the engine of this read-through architecture:

	@KeySpace("persons")
	@Entity
	@Table(name = "persons")
	public class Person implements Serializable {

    @Id
    @javax.persistence.Id
    private Long personId;
    private String name;
    private String surname;
    private String role;
    private Long teamId;

    @Column(name = "PERSON_ID")
    public Long getPersonId() {
        return personId;
    }

    public void setPersonId(Long personId) {
        this.personId = personId;
    }

    @Column(name = "NAME")
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column(name = "SURNAME")
    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    @Column(name = "ROLE")
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }

    @Column(name = "TEAM_ID")
    public Long getTeamId() {
        return teamId;
    }

    public void setTeamId(Long teamId) {
        this.teamId = teamId;
    }
	
**KeySpace** annotation here says that hazelcast will use IMap called "persons" for data to be held in-memory.

**Third step**

MapLoader implementation to glue the database and Hazelcast together:

    @Component
    public class HazelcastMapStore implements ApplicationContextAware, MapLoader<Long, Person> {

    private static PersonsJPARepository personsJPARepository;

    @Override
    public Person load(Long personId) {
        System.out.println("Loading by key: "+personId);
        return personsJPARepository.findById(personId).get();
    }

    @Override
    public Map<Long, Person> loadAll(Collection<Long> collection) {
        System.out.println("Loading collections of IDS: ");
        Map<Long, Person> result = new HashMap<>();
        for (Long key : collection) {
            Person productMap = this.load(key);
            if (productMap != null) {
                result.put(key, productMap);
            }
        }
        return result;
    }

    @Override
    public Iterable<Long> loadAllKeys() {
        System.out.println("Getting all the keys!");
        return personsJPARepository.findAllId();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        personsJPARepository = applicationContext.getBean(PersonsJPARepository.class);
    }

MapLoader ready, **now everytime when asked entity isn't in memory Hazelcast will hit MapLoader.load(personId)** Other two methods are just warm-up cache methods
to load the cached data before the first cache hit.

**Fourth step**

**create "persons" IMap** and connect it to just created **HazelcastMapStore**

    @Configuration
    public class HazelcastConfiguration {

    @Bean
    public Config hazelcastConfig(@Lazy HazelcastMapStore mapStore) {
        return new Config().setInstanceName("hazelcast-instance").addMapConfig(
                new MapConfig().setName("persons")
                        .setMapStoreConfig(
                                new MapStoreConfig().setEnabled(true).setInitialLoadMode(MapStoreConfig.InitialLoadMode.EAGER)
                                        .setImplementation(mapStore)
                        ));
    }

    @Bean
    public HazelcastInstance hazelcastInstance(Config config) {
        return Hazelcast.newHazelcastInstance(config);
    }

Oh and if having problems with wiring your Repository into your MapLoader then check this 
[Stackoverflow discussion](https://stackoverflow.com/questions/51958023/spring-boot-hazelcast-mapstore-cant-autowire-repository/52303755)

**Fifth step**

Configuring the Spring Data Repositories for Hazelcast (caching) and JPA (to access H2 DB)

    @SpringBootApplication(exclude = {
		HazelcastAutoConfiguration.class
    })
    @EnableHazelcastRepositories(basePackages={"com.example.hazelcast.demo.repositories.hz"})
    @EnableJpaRepositories(basePackages={"com.example.hazelcast.demo.repositories.jpa"})
    public class DemoApplication {

	    public static void main(String[] args) {
		    SpringApplication.run(DemoApplication.class, args);
	    }
    }

EnableHazelcastRepositories, EnableJpaRepositories annotations are just saying where the Spring Data repositories are located.  

Here is the full code:

    package com.example.hazelcast.demo.repositories.hz;

    import com.example.hazelcast.demo.model.Person;
    import org.springframework.data.hazelcast.repository.HazelcastRepository;

    public interface PersonsHazelcastRepository extends HazelcastRepository<Person, Long> {
        Person findPersonByPersonId(Long personId);
    }


    package com.example.hazelcast.demo.repositories.jpa;

    import com.example.hazelcast.demo.model.Person;
    import org.springframework.data.jpa.repository.Query;
    import org.springframework.data.repository.CrudRepository;

    public interface PersonsJPARepository extends CrudRepository<Person, Long> {
        @Query("SELECT n.id FROM Person n")
        Iterable<Long> findAllId();
    }

## Testing the read-through caching architecture 

First of all, I enabled Spring Data JPA sql logging so we're going to see if database has been asked for data.

    spring.jpa.show-sql=true
    spring.jpa.properties.hibernate.format_sql=true

Now to test the read-through caching I created very easy MVC Rest Controller which will be asking **only Hazelcast** 
for data. 

    import com.example.hazelcast.demo.model.Person;
    import com.example.hazelcast.demo.repositories.hz.PersonsHazelcastRepository;
    import org.springframework.web.bind.annotation.GetMapping;
    import org.springframework.web.bind.annotation.RequestParam;
    import org.springframework.web.bind.annotation.RestController;

    import javax.annotation.Resource;

    @RestController
    public class PersonsController {

        @Resource
        private PersonsHazelcastRepository personsHazelcastRepository;

        @GetMapping("/persons/query1")
        public Person getPersonByPersonId(@RequestParam("personId") Long personId) {
            return personsHazelcastRepository.findPersonByPersonId(personId);
        }
    }
    
Now test of application, compile the project first:

    A0057:demo Tomas.Kloucek$ mvn clean install

Second, run the Spring Boot app:

    A0057:demo Tomas.Kloucek$ java -jar target/demo-0.0.1-SNAPSHOT.jar

Open another terminal and get the data via REST call:

    A0057:demo Tomas.Kloucek$ curl http://localhost:8080/persons/query1?personId=1
    {"personId":1,"name":"Tomas","surname":"Kloucek","role":"Developer","teamId":1}

Now switch back to the terminal running the application and you should see that Hazelcast MapLoader has been asked for data:

    Getting all the keys!
    Hibernate: 
        select
            person0_.person_id as col_0_0_ 
        from
            persons person0_
    Loading collections of IDS: 
    Loading collections of IDS: 
    Loading by key: 2
    Loading collections of IDS: 
    Loading by key: 3
    Loading by key: 1
    Hibernate: 
        select
            person0_.person_id as person_i1_0_0_,
            person0_.name as name2_0_0_,
            person0_.role as role3_0_0_,
            person0_.surname as surname4_0_0_,
            person0_.team_id as team_id5_0_0_ 
        from
            persons person0_ 
        where
            person0_.person_id=?
    Hibernate: 
        select
            person0_.person_id as person_i1_0_0_,
            person0_.name as name2_0_0_,
        person0_.role as role3_0_0_,
        person0_.surname as surname4_0_0_,
        person0_.team_id as team_id5_0_0_ 
    from
        persons person0_ 
    where
        person0_.person_id=?
    Hibernate: 
        select
            person0_.person_id as person_i1_0_0_,
            person0_.name as name2_0_0_,
            person0_.role as role3_0_0_,
            person0_.surname as surname4_0_0_,
            person0_.team_id as team_id5_0_0_ 
        from
            persons person0_ 
        where
            person0_.person_id=?

Good, It works even with cache warm-up over MapLoader. Now try to get another person:

    A0057:demo Tomas.Kloucek$ curl http://localhost:8080/persons/query1?personId=2
    {"personId":2,"name":"Rudolf","surname":"Schoenecker","role":"Developer","teamId":1}
    
With switching back to the terminal running the application you should see that personId=2 has been returned from memory    
because there are no changes on console.

## Summary

I've got solid impressions from Spring Data Hazelcast. Works as intended. But poor documentation was a downer for me.

Cheers

Tomas

