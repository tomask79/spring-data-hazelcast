package com.example.hazelcast.demo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.hazelcast.HazelcastAutoConfiguration;
import org.springframework.data.hazelcast.repository.config.EnableHazelcastRepositories;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication(exclude = {
		HazelcastAutoConfiguration.class
})
@EnableHazelcastRepositories(basePackages={"com.example.hazelcast.demo.repositories.hz"})
@EnableJpaRepositories(basePackages={"com.example.hazelcast.demo.repositories.jpa"})
public class DemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(DemoApplication.class, args);
	}

}
