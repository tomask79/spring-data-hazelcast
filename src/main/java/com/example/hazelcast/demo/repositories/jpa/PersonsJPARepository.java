package com.example.hazelcast.demo.repositories.jpa;

import com.example.hazelcast.demo.model.Person;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

public interface PersonsJPARepository extends CrudRepository<Person, Long> {
    @Query("SELECT n.id FROM Person n")
    Iterable<Long> findAllId();
}
