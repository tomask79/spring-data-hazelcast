package com.example.hazelcast.demo.repositories.hz;

import com.example.hazelcast.demo.model.Person;
import org.springframework.data.hazelcast.repository.HazelcastRepository;

public interface PersonsHazelcastRepository extends HazelcastRepository<Person, Long> {
    Person findPersonByPersonId(Long personId);
}

