package com.example.hazelcast.demo.mvc;

import com.example.hazelcast.demo.model.Person;
import com.example.hazelcast.demo.repositories.hz.PersonsHazelcastRepository;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

@RestController
public class PersonsController {

    @Resource
    private PersonsHazelcastRepository personsHazelcastRepository;

    @GetMapping("/persons/query1")
    public Person getPersonByPersonId(@RequestParam("personId") Long personId) {
        return personsHazelcastRepository.findPersonByPersonId(personId);
    }
}
