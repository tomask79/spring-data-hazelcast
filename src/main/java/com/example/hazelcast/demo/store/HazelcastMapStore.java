package com.example.hazelcast.demo.store;

import com.example.hazelcast.demo.model.Person;
import com.example.hazelcast.demo.repositories.jpa.PersonsJPARepository;
import com.hazelcast.core.MapLoader;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

@Component
public class HazelcastMapStore implements ApplicationContextAware, MapLoader<Long, Person> {

    private static PersonsJPARepository personsJPARepository;

    @Override
    public Person load(Long personId) {
        System.out.println("Loading by key: "+personId);
        return personsJPARepository.findById(personId).get();
    }

    @Override
    public Map<Long, Person> loadAll(Collection<Long> collection) {
        System.out.println("Loading collections of IDS: ");
        Map<Long, Person> result = new HashMap<>();
        for (Long key : collection) {
            Person productMap = this.load(key);
            if (productMap != null) {
                result.put(key, productMap);
            }
        }
        return result;
    }

    @Override
    public Iterable<Long> loadAllKeys() {
        System.out.println("Getting all the keys!");
        return personsJPARepository.findAllId();
    }

    @Override
    public void setApplicationContext(ApplicationContext applicationContext) throws BeansException {
        personsJPARepository = applicationContext.getBean(PersonsJPARepository.class);
    }
}
