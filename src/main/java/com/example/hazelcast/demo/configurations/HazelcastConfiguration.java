package com.example.hazelcast.demo.configurations;

import com.example.hazelcast.demo.store.HazelcastMapStore;
import com.hazelcast.config.Config;
import com.hazelcast.config.MapConfig;
import com.hazelcast.config.MapStoreConfig;
import com.hazelcast.core.Hazelcast;
import com.hazelcast.core.HazelcastInstance;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Lazy;

@Configuration
public class HazelcastConfiguration {

    @Bean
    public Config hazelcastConfig(@Lazy HazelcastMapStore mapStore) {
        return new Config().setInstanceName("hazelcast-instance").addMapConfig(
                new MapConfig().setName("persons")
                        .setMapStoreConfig(
                                new MapStoreConfig().setEnabled(true).setInitialLoadMode(MapStoreConfig.InitialLoadMode.EAGER)
                                        .setImplementation(mapStore)
                        ));
    }

    @Bean
    public HazelcastInstance hazelcastInstance(Config config) {
        return Hazelcast.newHazelcastInstance(config);
    }
}
