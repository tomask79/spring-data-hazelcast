DROP TABLE IF EXISTS persons;

CREATE TABLE persons (
  PERSON_ID INT AUTO_INCREMENT  PRIMARY KEY,
  name VARCHAR(250) NOT NULL,
  surname VARCHAR(250) NOT NULL,
  role VARCHAR(250) DEFAULT NULL,
  TEAM_ID INT DEFAULT NULL
);

INSERT INTO persons (person_Id, name, surname, role, team_Id) VALUES
  (1, 'Tomas', 'Kloucek', 'Developer', 1),
  (2, 'Rudolf', 'Schoenecker', 'Developer', 1),
  (3, 'Roman', 'Fianta', 'TeamLeader', 1);
